# Rigel-based color scheme for Sublime Text

A dark color-scheme for Sublime Text based on the beautiful [Rigel](https://rigel.netlify.app/) theme.

## Installation

Copy the file `Rigel.sublime-color-scheme` in the `Packages/User` sub-folder. Then activate it using `Preferences->Color Scheme` (the name should be Rigel).

## Screenshots

### C/C++

![](img/Cpp.png)

### LaTeX

![](img/Latex.png)

### HTML

![](img/Html.png)